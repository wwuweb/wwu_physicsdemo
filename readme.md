#WWU Physics Department Classroom Demonstration Scheduling System#

This repository consists of the following components

1. A module, WWU Physics Demonstration Scheduler, which provides logic for form validation, updating the database after form submission, setting form default values, and displaying calendars.
2. A feature, WWU PhysicsDemo Pieces, which contains
	- A "Demonstration" content type and related "Equipment" taxonomy
	- Views:
		- Demonstration Catalog: a searchable table of demonstration nodes, links to a to-be-created reservation form (see "Setup")
		- Physics Reservation Calendar: displays field collection items attached to demonstration nodes (separate displays for staff and instructors)
		- Reservation Archives: displays field collection items attached to demonstration nodes (separate displays for staff and instructors)
			- Also contains a display for information about a single reservation, linked to from the calendar
	- Panel Pages:
		- Month, Day, and Week pages for the reservation calendar, use this as an example for how to implement by-role visibility for the catalog and archive pages
		- "Physics Reservations" which displays the individual reservation pages from the Archives view
	- A feed importer for importing demonstrations
3. A CSV file that contains all existing demonstrations to be imported
4. A folder of images, demonstration_images, for imported nodes

##Setup##

1. Clone repository and enable module and feature (fix content type non-exportable settings if desired)
2. Set up images for import. There are two options:
	1. Move demonstration_images folder to public files directory (sites/default/files) or 
	2. Add a Feeds Tamper plugin to the importer that will rewrite "public://" to some location where the images already exist on the web (e.g. "https://csedev.wwu.edu/sites/csedev.wwu.edu/files/")
3. Import demonstration nodes
4. Add catalog, calendar, and archives to sidebar/menu
	- Catalog & Archives: place provided views in panel layout (include visibility criteria by role/permission for archives)
    - Calendar: included in feature as panel pages (apply appropriate theme using Themekey)
5. Recreate reservation and cancellation forms (title and keys are important, see "Forms" below)
6. Apply appropriate theme to individual reservation pages using Themekey
7. Set up roles/permissions for appropriate users
	- Give the physics editor role permission to edit demonstrations

##Forms##

Make both forms "Physics" OG content.

###Reservation Form###

Title: "Demonstration Reservation Form"

Components:

- "demonstration\_name", textfield, default value "[current-page:query:demo\_name]", required
- "location", textfield, default value "[current-page:query:location]", required
- "course_number", textfield, default value "[current-page:query:course]"
- "date", date, default value "+1 day", required
- "start\_time", time, default value "[current-page:query:start-time]", required
- "end\_time", time, default value "[current-page:query:end-time]", required
- "comments", textarea, default value "[current-page:query:comments]"

Validation Rules (in this order):

- "Require Comments if Custom Demonstration" on demonstration\_name and comments
- "Compare Times" on date, start\_time, and end\_time
- "Time within range" on start\_time and end\_time with argument "8:00am 6:00pm"
- "Check reservation times" on demonstration\_name, date, start\_time, and end\_time

Other: Only allow authenticated users to submit

###Cancelation Form###

Title: "Cancel Reservation"

Components:

- "reservation\_id", hidden, default value "[current-page:query:reservation_id]"
- "confirm", markup, value "Are you sure you want to cancel your reservation for [current-page:query:demo\_name] on [current-page:query:reservation\_date]?"

Other: Only allow authenticated users to submit

##Copyright Statement##

Copyright 2015 Brian Lee, [Amerie Lommen](https://www.linkedin.com/in/ameriel), Dominic Pearson, Jacob Smullin, and Bridget Tueffers Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at https://opensource.org/licenses/ECL-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

This system was originally created as a Computer Science 49x sequence project.