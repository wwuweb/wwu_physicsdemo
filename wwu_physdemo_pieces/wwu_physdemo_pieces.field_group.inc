<?php
/**
 * @file
 * wwu_physdemo_pieces.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wwu_physdemo_pieces_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reservation_info|node|demonstration|form';
  $field_group->group_name = 'group_reservation_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'demonstration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reservation Info',
    'weight' => '5',
    'children' => array(
      0 => 'field_reservation_link',
      1 => 'field_reserved_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Reservation Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-reservation-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_reservation_info|node|demonstration|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Reservation Info');

  return $field_groups;
}
