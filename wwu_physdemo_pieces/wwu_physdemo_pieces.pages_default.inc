<?php
/**
 * @file
 * wwu_physdemo_pieces.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function wwu_physdemo_pieces_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'physics_reservation_calendar';
  $page->task = 'page';
  $page->admin_title = 'Physics Reservation Calendar (Month)';
  $page->admin_description = '';
  $page->path = 'physics/reservations-calendar/month/!view';
  $page->access = array();
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Month',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'view' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_physics_reservation_calendar__panel';
  $handler->task = 'page';
  $handler->subtask = 'physics_reservation_calendar';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'clean-25-75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Calendar';
  $display->uuid = 'f6bdb634-5066-4872-a6c3-a4bfe935a764';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-aa3ed4c8-15b7-459d-ac91-528095187fad';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'accordion_menu-14';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aa3ed4c8-15b7-459d-ac91-528095187fad';
    $display->content['new-aa3ed4c8-15b7-459d-ac91-528095187fad'] = $pane;
    $display->panels['left'][0] = 'new-aa3ed4c8-15b7-459d-ac91-528095187fad';
    $pane = new stdClass();
    $pane->pid = 'new-133c6057-bdb4-43d3-b4a9-2e7d20b99b5a';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<p>Please <a href="/user">log in</a> to view reservations.</p>
',
      'format' => 'clean_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '133c6057-bdb4-43d3-b4a9-2e7d20b99b5a';
    $display->content['new-133c6057-bdb4-43d3-b4a9-2e7d20b99b5a'] = $pane;
    $display->panels['right'][0] = 'new-133c6057-bdb4-43d3-b4a9-2e7d20b99b5a';
    $pane = new stdClass();
    $pane->pid = 'new-46b08a38-b24e-4bb5-8c9f-f218a4303a07';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/month',
      'display' => 'page_4',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '46b08a38-b24e-4bb5-8c9f-f218a4303a07';
    $display->content['new-46b08a38-b24e-4bb5-8c9f-f218a4303a07'] = $pane;
    $display->panels['right'][1] = 'new-46b08a38-b24e-4bb5-8c9f-f218a4303a07';
    $pane = new stdClass();
    $pane->pid = 'new-923307f6-91f2-483e-893c-5558a496d41d';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/month',
      'display' => 'page_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '923307f6-91f2-483e-893c-5558a496d41d';
    $display->content['new-923307f6-91f2-483e-893c-5558a496d41d'] = $pane;
    $display->panels['right'][2] = 'new-923307f6-91f2-483e-893c-5558a496d41d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-46b08a38-b24e-4bb5-8c9f-f218a4303a07';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['physics_reservation_calendar'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'physics_reservation_calendar_day_';
  $page->task = 'page';
  $page->admin_title = 'Physics Reservation Calendar (Day)';
  $page->admin_description = '';
  $page->path = 'physics/reservations-calendar/day/!view';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Day',
    'name' => 'navigation',
    'weight' => '3',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'view' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_physics_reservation_calendar_day___panel';
  $handler->task = 'page';
  $handler->subtask = 'physics_reservation_calendar_day_';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'clean-25-75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Calendar';
  $display->uuid = 'b69819a6-fb9a-450e-a041-b4715759c0e5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-cfc149ff-2e22-4cbf-84ef-4aaef73d5f67';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'accordion_menu-14';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cfc149ff-2e22-4cbf-84ef-4aaef73d5f67';
    $display->content['new-cfc149ff-2e22-4cbf-84ef-4aaef73d5f67'] = $pane;
    $display->panels['left'][0] = 'new-cfc149ff-2e22-4cbf-84ef-4aaef73d5f67';
    $pane = new stdClass();
    $pane->pid = 'new-efe12f33-c7f2-4046-8a3f-f6c862d48553';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/day',
      'display' => 'page_6',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'efe12f33-c7f2-4046-8a3f-f6c862d48553';
    $display->content['new-efe12f33-c7f2-4046-8a3f-f6c862d48553'] = $pane;
    $display->panels['right'][0] = 'new-efe12f33-c7f2-4046-8a3f-f6c862d48553';
    $pane = new stdClass();
    $pane->pid = 'new-c08b6f9b-543e-4757-99dd-d82cc0398b04';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/day',
      'display' => 'page_3',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c08b6f9b-543e-4757-99dd-d82cc0398b04';
    $display->content['new-c08b6f9b-543e-4757-99dd-d82cc0398b04'] = $pane;
    $display->panels['right'][1] = 'new-c08b6f9b-543e-4757-99dd-d82cc0398b04';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['physics_reservation_calendar_day_'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'physics_reservation_calendar_week_';
  $page->task = 'page';
  $page->admin_title = 'Physics Reservation Calendar (Week)';
  $page->admin_description = '';
  $page->path = 'physics/reservations-calendar/week/!view';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Week',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'view' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_physics_reservation_calendar_week___panel';
  $handler->task = 'page';
  $handler->subtask = 'physics_reservation_calendar_week_';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'clean-25-75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Calendar';
  $display->uuid = '21291dd5-d341-4a44-a60e-0f033af85b6b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f7711768-7bf9-4952-b60f-b2b9473aa081';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'accordion_menu-14';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7711768-7bf9-4952-b60f-b2b9473aa081';
    $display->content['new-f7711768-7bf9-4952-b60f-b2b9473aa081'] = $pane;
    $display->panels['left'][0] = 'new-f7711768-7bf9-4952-b60f-b2b9473aa081';
    $pane = new stdClass();
    $pane->pid = 'new-dc2b5597-f76a-4f4c-b86d-f1ebb1ebe473';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/week',
      'display' => 'page_5',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dc2b5597-f76a-4f4c-b86d-f1ebb1ebe473';
    $display->content['new-dc2b5597-f76a-4f4c-b86d-f1ebb1ebe473'] = $pane;
    $display->panels['right'][0] = 'new-dc2b5597-f76a-4f4c-b86d-f1ebb1ebe473';
    $pane = new stdClass();
    $pane->pid = 'new-eca30a8e-324d-4078-85c9-460ede5905fd';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'calendar';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'physics/reservations-calendar/week',
      'display' => 'page_2',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'eca30a8e-324d-4078-85c9-460ede5905fd';
    $display->content['new-eca30a8e-324d-4078-85c9-460ede5905fd'] = $pane;
    $display->panels['right'][1] = 'new-eca30a8e-324d-4078-85c9-460ede5905fd';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['physics_reservation_calendar_week_'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'physics_reservations';
  $page->task = 'page';
  $page->admin_title = 'Physics Reservations';
  $page->admin_description = '';
  $page->path = 'physics/reservations/!id';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'id' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_physics_reservations__panel';
  $handler->task = 'page';
  $handler->subtask = 'physics_reservations';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'clean-25-75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Reservation Details';
  $display->uuid = '516bad9c-fbf2-415f-ac5b-b5e599cd04e6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ead396c7-a80e-4354-8feb-cf3dc2cee163';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'accordion_menu-15';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ead396c7-a80e-4354-8feb-cf3dc2cee163';
    $display->content['new-ead396c7-a80e-4354-8feb-cf3dc2cee163'] = $pane;
    $display->panels['left'][0] = 'new-ead396c7-a80e-4354-8feb-cf3dc2cee163';
    $pane = new stdClass();
    $pane->pid = 'new-c28ee41f-568b-4af6-bfdf-da7c3f970175';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'reservation_archives';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'reservations',
      'display' => 'page_2',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c28ee41f-568b-4af6-bfdf-da7c3f970175';
    $display->content['new-c28ee41f-568b-4af6-bfdf-da7c3f970175'] = $pane;
    $display->panels['right'][0] = 'new-c28ee41f-568b-4af6-bfdf-da7c3f970175';
    $pane = new stdClass();
    $pane->pid = 'new-2b2f44cc-9cc3-459d-8a47-2677452b765c';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'reservation_archives';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 15,
              1 => 4,
              2 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => '',
      'url' => 'reservations',
      'display' => 'page_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '2b2f44cc-9cc3-459d-8a47-2677452b765c';
    $display->content['new-2b2f44cc-9cc3-459d-8a47-2677452b765c'] = $pane;
    $display->panels['right'][1] = 'new-2b2f44cc-9cc3-459d-8a47-2677452b765c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['physics_reservations'] = $page;

  return $pages;

}
